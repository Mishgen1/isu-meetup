import { Component, OnInit } from '@angular/core';
import { meetupList } from './meetup';
import { MeetUpEvent } from './meetup.interface';
import { MeetupService } from './meetup.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent{
  title = 'ISU-meetup';
}

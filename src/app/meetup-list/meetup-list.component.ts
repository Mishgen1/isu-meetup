import { Component, OnInit } from '@angular/core';

import { meetupList } from '../meetup';
import { MeetUpEvent } from '../meetup.interface';
import { MeetupService } from '../meetup.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-meetup-list',
  templateUrl: './meetup-list.component.html',
  styleUrls: ['./meetup-list.component.scss']
})
export class MeetupListComponent implements OnInit {
  title = 'Meetups list';
  /** список событий */
  meetupList: MeetUpEvent[];

  constructor(private service: MeetupService){}

  ngOnInit():void{
    this.meetupList = this.service.getAll();
  }
  
  deleteMeetup(id: number) {
    this.service.deleteMeetup(id);
    this.meetupList = this.meetupList.filter(m => m.id !== id);
  }
  
  onSubmit(v: NgForm){
    this.service.addMeetup(v.value);
    this.meetupList = this.service.getAll();
  //  console.log(v.value);
  }
}

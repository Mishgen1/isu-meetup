import { Injectable } from '@angular/core';
import { meetupList } from './meetup';
import { MeetUpEvent } from './meetup.interface';

@Injectable({
  providedIn: 'root'
})
export class MeetupService {
  private meetupList: MeetUpEvent[]=meetupList;

  getAll():MeetUpEvent[]{
    return this.meetupList.slice();
  }

  deleteMeetup(id: number){
    this.meetupList = this.meetupList.filter(m => m.id !== id);
  }

  addMeetup(meetup: MeetUpEvent){
    meetup.id = Date.now();
    this.meetupList.push(meetup);
  }
  
  constructor() { }
}
